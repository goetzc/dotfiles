-- mpv script to show the progress bar on pause. --

function on_pause(name, value)
  if value == true then
    mp.command("show-progress")
    -- sleep(5)
    -- mp.command("hide-progress")
  end
end

-- test sleep
function sleep(n)
  os.execute("sleep " .. tonumber(n))
end

-- Show progress bar on pause
-- mp.observe_property("pause", "bool", on_pause)
