-- Script to download subtitles.
-- default keybinding: B  (capital B)
-- add the following to your input.conf to change the default keybinding:
-- keyname script_binding auto_load_subs

local utils = require 'mp.utils'

-- https://gist.github.com/selsta/ce3fb37e775dbd15c698
function subliminal()
    subl = "/usr/bin/subliminal" -- use 'which subliminal' to find the path
    -- language = "en"
    language = "es"

    log("Searching " .. string.upper(language) .. " subtitles...")
    t = {}
    t.args = {
        subl,
        "download",
        "--single",
        "--language", language,
        mp.get_property("path")
    }
    res = utils.subprocess(t)

    rescan_external_files(res)
end

-- The subdl backend requieres login updates for the provider
-- requires subdl (2016 Sep master branch): https://github.com/alexanderwink/subdl
function subdl()
    subdl = "/usr/bin/subdl" -- use 'which subdl' to find the path.
    language = "eng"
    download = "best-rating" --  Other options: first, most-downloaded

    log("Searching " .. string.upper(language) .. " subtitles...")
    t = {}
    t.args = {
        subdl,
        "--lang=" .. language,
        "--download=" .. download,
        mp.get_property("path")
    }
    res = utils.subprocess(t)

    rescan_external_files(res)
end

function rescan_external_files(res)
    if res.status == 0 then
        mp.commandv("rescan_external_files", "reselect")
        log("Subtitle download succeeded.")
    else
        log("Subtitle download failed!")
    end
end

-- Log function: log to both terminal and MPV OSD (On-Screen Display)
function log(string, sec)
    sec = sec or 2.5            -- defaults to 2.5 when sec parameter is absent
    mp.msg.warn(string)         -- This logs to the terminal
    mp.osd_message(string, sec) -- This logs to MPV screen
end


-- Add the keybinding for the function
mp.add_key_binding("B", "auto_load_subs", subliminal)
-- mp.add_key_binding("B", "auto_load_subs", subdl)
