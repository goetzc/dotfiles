#!/usr/bin/env sh

set -eu

ARTIST=$(playerctl metadata artist)
TITLE=$(playerctl metadata title)

if [ -n "$ARTIST" ]; then
    echo "$ARTIST - $TITLE"
else
    echo "$TITLE"
fi
