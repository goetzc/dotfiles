##################################################
# Common shell settings
# https://shreevatsa.wordpress.com/2008/03/30/zshbash-startup-files-loading-order-bashrc-zshrc-etc/
#
# For Bash, stuff will go into ~/.bashrc, and ~/.bash_profile will source it.
# For Zsh, stuff will go into ~/.zshrc, which is always executed.
##################################################

# Safe source
source_() {
  if [ -e ${1} ]; then
    source ${1}
  fi
}

# Check whether the given command exists
has_cmd() {
  command -v "$1" >/dev/null 2>&1
}

# Safe alias
alias_() {
  local cmd_name
  cmd_name=$(echo "$2" | awk '{print $1}')

  if has_cmd "$cmd_name"; then
    alias "$1"="$2"
  else
    echo "Command '$cmd_name' not found. Alias '$1' not created."
  fi
}

# macOS check
is_macos() {
  if [[ "$OSTYPE" == "darwin"* ]]; then
    true
  else
    false
  fi
}

# Wayland check
is_wayland() {
  if [[ "$XDG_SESSION_TYPE" == wayland ]]; then
    true
  else
    false
  fi
}

add_to_path_l() {
  if [[ ! "${PATH}" =~ "$1" ]]; then
    export PATH="$1:${PATH}"
  fi
}

add_to_path_r() {
  if [[ ! "${PATH}" =~ "$1" ]]; then
    export PATH="${PATH}:$1"
  fi
}

add_to_manpath_l() {
  if [[ ! "${MANPATH}" =~ "$1" ]]; then
    export MANPATH="$1:${MANPATH:-/usr/share/man}"
  fi
}

# Download YouTube audio file form a video
dla() {
  youtube-dl --no-playlist --format 140 "${1}" # M4A audio-only
}

# Show color rainbow
color_rainbow() {
  sh -c "$(curl -fsSL https://gitlab.com/snippets/1970716/raw)"
}

# Credentials file, not stored by Git.
source_ ~/.credentials/env.sh

# Command-Not-Found hook
source_ /usr/share/doc/pkgfile/command-not-found.zsh

# OrbStack
source_ ~/.orbstack/shell/init.zsh

### Colorful shell
export GREP_COLORS="mt=1;33"
export LESS="--RAW-CONTROL-CHARS --ignore-case --HILITE-UNREAD --quiet --tabs=4 --window=-2"
export LESSOPEN="|$(which lesspipe.sh) %s" LESS_ADVANCED_PREPROCESSOR=1
alias grep='grep --color=auto'
if is_macos; then
  alias ls='ls -G -h -F -l'
else
  alias ls='ls --color=auto --human-readable --group-directories-first --classify'
  alias dmesg='dmesg --color'
fi

man() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m") \
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[4;32m") \
    PAGER="${commands[less]:-$PAGER}" \
    _NROFF_U=1 \
    GROFF_NO_SGR=1 \
    man "$@"
}

# SSH agent and directories
ssh_settings() {
  mkdir -p ~/.ssh/sockets

  if is_macos; then
    agent_file="$HOME/.ssh/agent-output.sh"
    if ! pgrep -u "$USER" ssh-agent >/dev/null; then
      ssh-agent >"$agent_file"
      sed -i "" "/echo /d" "$agent_file"
    fi
    if [[ "$SSH_AGENT_PID" == "" ]]; then
      eval "$(<$agent_file)"
    fi
  else
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
  fi
}
ssh_settings

## PATH
if is_macos; then
  eval "$(/opt/homebrew/bin/brew shellenv)"
  add_to_path_l "/usr/local/opt/coreutils/libexec/gnubin"
  add_to_manpath_l "/usr/local/opt/coreutils/libexec/gnuman"
fi
add_to_path_l ~/.local/bin
add_to_path_r "$HOME/.cargo/bin"
add_to_path_r "$HOME/Documents/software/code/shell"
add_to_path_r "$HOME/Documents/software/code/python"

###################################################################
##                             Helpers                           ##
###################################################################

STARSHIP_SHELL="$(ps -p $$ -o comm= | tr -d '-')"
if ! has_cmd starship; then
  echo "Installing Starship..."
  curl -fsSL https://starship.rs/install.sh |
    sh -s -- --yes --bin-dir="$HOME/.local/bin"
else
  eval "$(starship init "${STARSHIP_SHELL}")"
fi

if [ ! -d ~/.asdf ]; then
  echo "Cloning asdf-vm..."
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf
  cd ~/.asdf && git checkout "$(git describe --abbrev=0 --tags)"
  cd ~/
else
  source_ $HOME/.asdf/asdf.sh
  # source_ $HOME/.asdf/completions/asdf.bash
fi

if [ ! -d ~/.tmux ]; then
  echo "Cloning Tmux configuration..."
  git clone https://github.com/gpakosz/.tmux.git ~/.tmux
  echo -e "\nCreating symlinks..."
  ln -s ~/.tmux/.tmux.conf ~/
  ln -s ~/.dotfiles/tmux/.tmux.conf.local ~/
fi

if [ ! -d ~/.emacs.d ]; then
  echo "Cloning Doom Emacs configuration..."
  git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
fi

##### Environment variables

export GPG_TTY="$(tty)"
export GPGKEY=5D5EC1BA
export CIRCLECI_CLI_SKIP_UPDATE_CHECK=true
# export EDITOR='emacsclient --alternate-editor='
export EDITOR=nvim
export VISUAL=$EDITOR

##### Alias
# To force any command prepend a \slash.
if is_macos; then
  alias copy=pbcopy

  alias_ rm 'grm -I'
else
  if is_wayland; then
    alias copy='wl-copy'
  else
    alias copy='xclip -sel clip'
  fi
  alias open=xdg-open

  alias rm='rm -I'
fi
alias df='df -h'
alias du='du -h'
alias fd="fd --hidden"
alias free='free -h'
alias h='history'
alias rg="rg --hidden --glob '!.git/**'"
alias sudo='sudo '
has_cmd eza && alias l='eza -l' || alias l='ls -lh'
has_cmd eza && alias la='eza -la' || alias la='ls -lah'
alias_ cat bat
alias_ diff colordiff
alias_ pip pip3
alias_ vim nvim
alias_ k kubectl
alias_ kx kubectx
alias_ tf terraform
alias cabal='echo Sure? This is cabal'
alias chatgpt='export OPENAI_KEY="$(~/.credentials/openai.sh)" && chatgpt'
alias e='emacsclient --no-wait --create-frame --alternate-editor='
alias et='emacsclient -tty -a ""'
alias ghci='~/Documents/software/code/haskell/ghci-color/ghci-color'
alias mpvi='mpv -profile image'
alias rsync='rsync --progress --stats'
alias vi='\vim'
alias preview="loupe"
alias ssha='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
