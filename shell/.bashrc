###################################################################
##                         User configuration                    ##
###################################################################

# Without considering (non-)interactive shells, ~/.bash_profile is sourced only
# by login shells, and ~/.bashrc is source directly by non-login shells, so we
# need to manualy source ~/.profile on non-login shells.
# https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html

# [[ $- == *i* ]] && echo 'Interactive' || echo 'Non-interactive'
# shopt -q login_shell && echo 'Login' || echo 'Non-login'

shopt -q login_shell || [[ -f ~/.profile ]] && . ~/.profile

### Check for an interactive session
interactive_session() {
    [ -z "$PS1" ] && return
    black=$(tput setaf 0)
    red=$(tput setaf 1)
    green=$(tput setaf 2)
    yellow=$(tput setaf 3)
    blue=$(tput setaf 4)
    purple=$(tput setaf 5)
    cyan=$(tput setaf 6)
    white=$(tput setaf 7)
    grey=$(tput setaf 8)
    reset=$(tput sgr0)
    # PS1="$PS1\n > " # newline and ' > '
    PS1="\[$red\]\$? \[$cyan\]\u\[$reset\]@\[$green\]\h:\[$yellow\]\w\[$reset\]$ "
    eval `dircolors -b`
}
interactive_session

###############
#### Shell ####
###############

HISTCONTROL=ignoreboth
HISTSIZE=5000
