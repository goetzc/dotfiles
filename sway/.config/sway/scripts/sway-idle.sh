#!/usr/bin/env sh

screen_on="swaymsg 'output * dpms on'"
screen_off="swaymsg 'output * dpms off'"
screen_dim="$HOME/.config/sway/scripts/dim-screen-brightness.sh"
screen_lock="$HOME/.config/sway/scripts/sway-lock-screen.sh"
player_pause="playerctl pause"
system_sleep="echo 'Sleeping system...'; systemctl suspend"

# All timeouts start counting at the program start.
swayidle -w                                       \
    timeout 360  "${screen_dim} s d"              \
         resume  "${screen_on};  ${screen_dim} r" \
    timeout 720  "${screen_off}; ${screen_lock}"  \
         resume  "${screen_on};  ${screen_dim} r" \
    timeout 1800 "${system_sleep}"                \
    before-sleep "${player_pause}; ${screen_lock}"
