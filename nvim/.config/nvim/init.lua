-- Bootstrap lazy.nvim, LazyVim and custom plugins
require("config.lazy")

local configuration_path = vim.fn.stdpath("config") .. "/"

vim.api.nvim_create_user_command("EditConfig", ":e " .. configuration_path .. "init.lua", {})
vim.fn.execute("source " .. configuration_path .. "abbreviations.vim")
