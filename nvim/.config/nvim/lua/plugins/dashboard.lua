return {
  {
    "folke/snacks.nvim",
    opts = {
      dashboard = {
        enabled = true,
        config = function(opts)
          table.insert(opts.preset.keys, 2, {
            hidden = true,
            icon = " ",
            key = "i",
            desc = "New File",
            action = ":ene | startinsert",
          })
        end,
      },
    },
  },
}
