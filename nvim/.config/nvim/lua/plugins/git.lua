return {
  {
    -- Generate file permalinks for several git web frontend hosts
    "ruifm/gitlinker.nvim",
    lazy = true,
    event = "BufRead",
    dependencies = "nvim-lua/plenary.nvim",
    config = function()
      require("gitlinker").setup({
        opts = {
          action_callback = require("gitlinker.actions").open_in_browser,
          print_url = true,
          mappings = "<Leader>gy",
        },
      })
    end,
  },

  {
    -- GitHub
    "pwntester/octo.nvim",
    event = "VeryLazy",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "ibhagwan/fzf-lua",
      "nvim-tree/nvim-web-devicons",
    },
    config = function()
      require("octo").setup()
    end,
  },
}
