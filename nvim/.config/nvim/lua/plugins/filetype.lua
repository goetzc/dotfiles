return {
  {
    "yasuhiroki/circleci.vim",
    event = { "BufEnter *.yml", "BufEnter *.yaml" },
    ft = "yaml",
    init = function()
      vim.g.circleci_omnifunc_enable = 1
    end,
  },
  {
    "towolf/vim-helm",
    lazy = false,
    ft = "yaml",
  },
  {
    "pearofducks/ansible-vim",
    lazy = false,
    init = function()
      vim.g.ansible_extra_keywords_highlight = 1
    end,
  },
  -- {
  --   "mfussenegger/nvim-ansible",
  -- },
  {
    "Glench/Vim-Jinja2-Syntax",
    lazy = false,
  },
}
