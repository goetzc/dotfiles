return {
  {
    -- GnuPG integration
    "jamessan/vim-gnupg",
    lazy = false,
    config = false,
  },
}
