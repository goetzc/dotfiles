return {
  {
    "chrisgrieser/nvim-genghis",
    lazy = false,
    dependencies = {
      "snacks.nvim", -- vim.ui.input
      "famiu/bufdelete.nvim", -- Optional
    },
  },
}
