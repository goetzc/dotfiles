return {
  -- Color scheme / Theme
  {
    "navarasu/onedark.nvim",
    -- lazy = not vim.startswith(vim.g.colors_name, "onedark"),
    lazy = false,
    config = function()
      require("onedark").setup({
        transparent = vim.env.TMUX ~= nil,
      })
      require("onedark").load()
    end,
  },

  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "onedark",
    },
  },

  {
    "rcarriga/nvim-notify",
    enabled = false,
  },

  {
    "folke/noice.nvim",
    opts = {
      messages = {
        enabled = false,
      },
      presets = {
        bottom_search = true,
        command_palette = false,
        long_message_to_split = false,
        inc_rename = true,
      },
    },
  },

  {
    -- Highlight `todo` kind of words
    "folke/todo-comments.nvim",
    -- lazy = true,
    -- event = "BufRead",
    -- dependencies = "nvim-lua/plenary.nvim",
    opts = {
      highlight = {
        pattern = [[.*<(KEYWORDS)\s*]],
      },
    },
  },
}
