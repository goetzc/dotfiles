-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua

vim.api.nvim_create_autocmd("FileType", {
  desc = "Text oriented file type settings",
  group = vim.api.nvim_create_augroup("text_oriented_files", { clear = true }),
  pattern = {
    "gitcommit",
    "markdown",
    "plaintex",
    "text",
  },
  callback = function()
    vim.opt_local.wrap = true
    vim.opt_local.spell = true
  end,
})
