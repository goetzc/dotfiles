-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua

require("config.gui")

local time = os.date("*t")

vim.opt.clipboard = ""
vim.opt.completeopt = "menu,menuone,noselect"
vim.opt.relativenumber = true
vim.opt.wrap = true
-- vim.opt.inccommand = 'split'
-- vim.opt.shiftwidth = 2
-- vim.opt.scrolloff = 5
-- vim.opt.sidescrolloff = 5
-- vim.opt.tabstop = 2

-- -- Color scheme
-- if (time.hour >= 17) or (time.hour < 8) then
--   -- vim.cmd.colorscheme("kanagawa-dragon")
--   vim.cmd.colorscheme("onedark")
-- else
--   vim.cmd.colorscheme("peachpuff")
-- end
-- -- vim.g.colors_name
