-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua

-- For info on the different modes:
-- :h map-modes
-- :h keycodes

local Util = require("lazyvim.util")

vim.keymap.set("n", "<A-x>", "<cmd>Telescope commands<CR>") -- À la Emacs: Alt-x

-- Make some shortcuts effective to the end of line
vim.keymap.set("n", "Y", "y$")
vim.keymap.set("n", "vC", "v$h")

-- Blockwise indent
vim.keymap.set("n", ">", ">>", { desc = "Blockwise indent right" })
vim.keymap.set("n", "<", "<<", { desc = "Blockwise indent left" })
vim.keymap.set("v", ">", ">gv", { desc = "Blockwise indent right" })
vim.keymap.set("v", "<", "<gv", { desc = "Blockwise indent left" })

-- Clipboard
vim.keymap.set("v", "<C-y>", '"+y', { desc = "Copy to system clipboard" })
vim.keymap.set("i", "<C-p>", "<C-r>+", { desc = "Paste from system clipboard" })

-- Buffers
vim.keymap.set("n", "<Leader><Tab>", "<cmd>e #<CR>", { desc = "Switch to other buffer" })
vim.keymap.set("n", "<Leader>bn", "<cmd>BufferLineCycleNext<CR>", { desc = "Next" })
vim.keymap.set("n", "<Leader>bp", "<cmd>BufferLineCyclePrev<CR>", { desc = "Previous" })
vim.keymap.set("n", "<Leader>bP", "<cmd>BufferLineTogglePin<CR>", { desc = "Toggle pin" })
vim.keymap.set("n", "<Leader>z", "<cmd>Bdelete<CR>", { desc = "Close buffer" })
vim.keymap.set("n", "<Leader>[", "<cmd>BufferLineCyclePrev<CR>", { desc = "Previous buffer" })
vim.keymap.set("n", "<Leader>]", "<cmd>BufferLineCycleNext<CR>", { desc = "Next buffer" })

-- Splits/Windows
vim.keymap.set("n", "<Leader>\\", "<cmd>vsplit<CR>", { desc = "Split window right" })
vim.keymap.set("n", "<Leader>w\\", "<cmd>vsplit<CR>", { desc = "Split window right" })
vim.keymap.set("n", "<Leader>w-", "<cmd>split<CR>", { desc = "Split window below" })
---- Resize (Shift-Alt)
vim.keymap.set("n", "<S-A-=>", "<C-w>=", { desc = "Set equal window sizes" })
vim.keymap.set("n", "<S-A-k>", "<cmd>resize ' . (winheight(0) + 2)<CR>", { desc = "Increase window size" })
vim.keymap.set("n", "<S-A-j>", "<cmd>resize ' . (winheight(0) - 2)<CR>", { desc = "Decrease window size" })
vim.keymap.set("n", "<S-A-h>", "<cmd>vertical resize ' . string(winwidth(0) * 1.05)<CR>", { desc = "Resize window" })
vim.keymap.set("n", "<S-A-l>", "<cmd>vertical resize ' . string(winwidth(0) * 0.95)<CR>", { desc = "Resize window" })

-- Files
vim.keymap.set({ "i", "x", "n", "s" }, "<Esc>⌘s", "<cmd>w<CR><Esc>", { desc = "Save file" })
vim.keymap.set("n", "<Leader>fD", "<cmd>Genghis trashFile<CR>", { desc = "Trash file" })
vim.keymap.set("n", "<Leader>ff", "<cmd>Telescope file_browser<CR>", { desc = "Broser files" })
vim.keymap.set("n", "<Leader>fr", "<cmd>Genghis renameFile<CR>", { desc = "Rename file" })
vim.keymap.set("n", "<Leader>fS", ":execute 'saveas' expand('%:h') . '/' . ''<Left>", { desc = "Safe file as" })
vim.keymap.set("n", "<Leader>fp", vim.cmd.EditConfig, { desc = "Edit configuration" })
vim.keymap.set("n", "<Leader>W", "<cmd>noautocmd w<CR>", { desc = "Save without formatting" })
vim.keymap.set("n", "<Leader>fy", function()
  local path = vim.fn.expand("%")
  vim.fn.setreg("+", path)
  print("Copied path: " .. path)
end, { desc = "Yank file path from project" })
vim.keymap.set("n", "<C-p>", function()
  require("telescope.builtin").git_files({ show_untracked = true })
end, { desc = "Find Git tracked files" })
vim.keymap.set("n", "<Leader>fz", function()
  require("telescope.builtin").find_files({ cwd = require("lazy.core.config").options.root })
end, { desc = "Find LazyVim file" })
vim.keymap.set(
  "c",
  "w!!",
  "execute 'silent! write !SUDO_ASKPASS=`which askpass` sudo tee % >/dev/null' <bar> edit!",
  { desc = "Write file as sudo" }
)

-- Search
vim.keymap.set("n", "<Leader>sp", "<cmd>Telescope live_grep<CR>", { desc = "Text" })
vim.keymap.set("n", "<Leader>sP", "<cmd>Telescope projects<CR>", { desc = "Projects" })
vim.keymap.set("n", "<Leader>s?", "<cmd>Telescope current_buffer_fuzzy_find<CR>", { desc = "Fuzzy find" })
vim.keymap.set("n", "<Leader>S", ":%s//g<Left><Left>", { noremap = true, desc = "Substitute text" })
vim.keymap.set(
  "n",
  "<A-s>",
  [[:%s/\<<C-R>=expand('<cword>')<CR>\>//g<Left><Left>]],
  { noremap = true, desc = "Replace word" }
)
vim.keymap.set("v", "<Leader>S", ":s//g<Left><Left>", { desc = "Substitute text" })
vim.keymap.set("v", "<Leader>/", [[y:<C-u>execute '/'.escape(@", '/\')<CR>]], { noremap = true })
vim.keymap.set("n", "<Leader>*", "<cmd>Telescope grep_string<CR>", { desc = "Search word under cursor" })
vim.keymap.set("n", "<BS>", "<cmd>nohlsearch<CR>h")

-- Floating Terminal
local lazyterm = function()
  Util.terminal(nil, { cwd = Util.root() })
end
vim.keymap.set("n", "<C-\\>", lazyterm, { desc = "Terminal (root dir)" })
vim.keymap.set("t", "<C-\\>", "<cmd>close<CR>", { desc = "Hide terminal" })

-- Insert
vim.keymap.set("n", "<Leader>i", "", { desc = "Insert" })
vim.keymap.set("n", "<Leader>iF", "i<C-r>=expand('%')<CR>", { desc = "Insert file name" })
vim.keymap.set("n", "<Leader>if", "i<C-r>=expand('%:t')<CR>", { desc = "Insert full file name" })

-- Comments
vim.keymap.set("n", "<C-/>", "gcc", { remap = true, silent = true, desc = "Comment line" })
vim.keymap.set("x", "<C-/>", "gc", { remap = true, silent = true, desc = "Comment selection" })
---- C-_ means C-/ in old terminals emulation
vim.keymap.set("n", "<C-_>", "gcc", { remap = true, silent = true, desc = "Comment line" })
vim.keymap.set("x", "<C-_>", "gc", { remap = true, silent = true, desc = "Comment selection" })

-- Git
vim.keymap.set("n", "<Leader>ga", function()
  require("gitsigns").status()
end, { desc = "Status" })
vim.keymap.set("n", "<Leader>gs", "<cmd>Gitsigns stage_hunk<CR>", { desc = "Stage hunk" })
vim.keymap.set("n", "<Leader>gS", "<cmd>Gitsigns stage_buffer<CR>", { desc = "Stage buffer" })
vim.keymap.set("n", "<Leader>gu", "<cmd>Gitsigns undo_stage_hunk<CR>", { desc = "Unstage hunk" })
vim.keymap.set("n", "<Leader>gU", "<cmd>Gitsigns undo_stage_buffer<CR>", { desc = "Unstage buffer" })
vim.keymap.set("n", "<Leader>gb", "<cmd>Gitsigns blame_line<CR>", { desc = "View blame" })
vim.keymap.set("n", "<Leader>gB", "<cmd>Gitsigns blame<CR>", { desc = "View full blame" })
vim.keymap.set("n", "<Leader>gi", "<cmd>Octo issue list<CR>", { desc = "GitHub issues" })
vim.keymap.set("n", "<Leader>gP", "<cmd>Octo pr list<CR>", { desc = "GitHub PRs" })

-- Help
vim.keymap.set("n", "<Leader>h", "", { desc = "Help" })
vim.keymap.set("n", "<Leader>hc", "<cmd>Telescope commands<CR>", { desc = "Commands" })
vim.keymap.set("n", "<Leader>hh", "<cmd>Telescope help_tags<CR>", { desc = "Help tags" })
vim.keymap.set("n", "<Leader>hH", "<cmd>Telescope highlights<CR>", { desc = "Highlight groups" })
vim.keymap.set("n", "<Leader>hk", "<cmd>Telescope keymaps<CR>", { desc = "Keymaps" })
vim.keymap.set("n", "<Leader>hm", "<cmd>Telescope man_pages<CR>", { desc = "Man pages" })
vim.keymap.set("n", "<Leader>ho", "<cmd>Telescope old_files<CR>", { desc = "Recent files" })
vim.keymap.set("n", "<Leader>hr", "<cmd>Telescope registers<CR>", { desc = "Registers" })
vim.keymap.set("n", "<Leader>ht", function()
  require("telescope.builtin").colorscheme({ enable_preview = true })
end, { desc = "Themes" })

-- map <leader>p :w <Bar> Vy:!zathura <C-r>" & <CR><CR>
