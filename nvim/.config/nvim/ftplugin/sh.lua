-- Maps
vim.keymap.set("n", "<Leader>r", "<cmd>w <Bar> :!bash %<CR>")
vim.keymap.set("n", "<Leader>cz", "<cmd>!shellcheck -x %<CR>")
