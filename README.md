# Dotfiles

This is my collection of user configuration files, dotfiles, you name it.

## Table of Contents

<!-- toc -->

- [Usage](#usage)
- [License](#license)

<!-- tocstop -->

## Usage

Pull the repository, and then create the symbolic links
[using](https://alexpearce.me/2016/02/managing-dotfiles-with-stow/) GNU
[Stow](https://www.gnu.org/software/stow/).

You can list all directories and files with:

```sh
tree -a -I "\.git"
```

To use it, execute `stow` using the directories as arguments, e.g.:

```sh
stow shell emacs haskell
```

## License

Unless stated otherwise, all files are licensed under the
[MIT](https://opensource.org/licenses/MIT) License.
