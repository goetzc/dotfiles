;;;; .doom.d/config.el -*- lexical-binding: t; -*-
;;
;; These produce the same result:
;; (bind-key "s-/" #'cool-function)
;; (define-key global-map (kbd "s-/") #'cool-function)
;; (global-set-key (kbd "s-/") #'cool-function)


;;;; -- Themes & Fonts -- ;;;;

;; Theme
(if window-system
    ;; (setq doom-theme 'doom-gruvbox)
    (setq doom-theme 'doom-one)
    ;; (setq doom-theme 'wombat)
    ;; (setq doom-theme 'doom-solarized-light)
    ;; (setq doom-theme 'doom-oceanic-next)
  ;; (setq doom-theme 'doom-acario-dark)
  ;; (setq doom-theme 'doom-gruvbox-light)
  (setq doom-theme 'doom-gruvbox)
)

;; Fonts
(setq doom-font
      (font-spec :family "Source Code Pro"
                 :size (if window-system
                           (if (>= (x-display-pixel-width) 1440)
                               (pcase system-type
                                 ('gnu/linux 14.5)
                                 ('darwin 15.5)
                                 )
                             10.5)
                         14.5 ;; Daemon
                         )
                 :weight 'normal))


;;;; -- Ivy -- ;;;;

;; RegEx builder
(setq ivy-re-builders-alist
      '((read-file-name-internal . ivy--regex-plus)
        (t . ivy--regex-fuzzy)))


;;;; -- Lines -- ;;;;

(setq display-line-numbers-type 'relative)
(global-visual-line-mode t)
;; (+global-word-wrap-mode +1)

;; Scroll one line at a time, three with shift modifier
(setq mouse-wheel-scroll-amount
      '(1 ((shift) . 3)))

;; Move lines up and down
(map! :map evil-normal-state-map
      "M-K" #'drag-stuff-up
      "M-J" #'drag-stuff-down
      )


;;;; -- Windows -- ;;;;

;; Resize
(bind-key "M-k" 'enlarge-window)
(bind-key "M-j" 'shrink-window)
(bind-key "M-l" 'enlarge-window-horizontally)
(bind-key "M-h" 'shrink-window-horizontally)

;; Split
(setq evil-split-window-below t)
(setq evil-vsplit-window-right t)
(map! :leader
      :desc "Split window horizontally" "w -"    #'evil-window-split
      :desc "Split window vertically"   "w /"    #'evil-window-vsplit
      :desc "Split window vertically"   "w \\"   #'evil-window-vsplit
      )


;;;; -- Registers & Clipboard -- ;;;;

;; Shotcuts
(setq select-enable-clipboard nil)
(define-key evil-insert-state-map  (kbd "M-v") (kbd "+"))
(define-key evil-insert-state-map  (kbd "C-p") (kbd "+"))
(define-key evil-ex-completion-map (kbd "C-p") (kbd "+"))
(define-key evil-ex-search-keymap  (kbd "C-p") (kbd "+"))
(define-key evil-visual-state-map  (kbd "C-y") (kbd "\"+y"))

;; Replace with register
(setq evil-replace-with-register-key (kbd "gr"))
(evil-replace-with-register-install)

;; Paste-transient-state: cycle through kill ring on paste
(when (modulep! :ui hydra)
  (defhydra hydra-paste (:color red
                         :hint nil)
    (concat "\n[%s(length kill-ring-yank-pointer)/%s(length kill-ring)]\t"
            "[_C-j_/_C-k_] cycles through yanked text; [_p_/_P_] pastes the same text"
            "above or below. Anything else exits.")
    ("C-j" evil-paste-pop)
    ("C-k" evil-paste-pop-next)
    ("p" evil-paste-after)
    ("P" evil-paste-before))
  (map! :nv "p" #'hydra-paste/evil-paste-after
        :nv "P" #'hydra-paste/evil-paste-before)
  )


;;;; -- General Shortcuts -- ;;;;

;; Overrides
(map! :leader
      :desc "Workspace leader key"  "`"     doom-leader-workspace-map
      :desc "Switch to last buffer" "TAB"   #'evil-switch-to-windows-last-buffer
      :desc "Open Term"             "'"     #'+vterm/toggle
      :desc "Resume last search"    "\""
      (cond ((modulep! :completion vertico)   #'vertico-repeat)
            ((modulep! :completion ivy)       #'ivy-resume)
            ((modulep! :completion helm)      #'helm-resume))
      )

;; Common
(define-key evil-normal-state-map (kbd "C-s") 'save-buffer)
(define-key evil-normal-state-map (kbd "C-/") 'evilnc-comment-or-uncomment-lines)
(define-key evil-normal-state-map (kbd "C-p") '+ivy/projectile-find-file)
(define-key evil-visual-state-map (kbd "C") (kbd "$h"))

;; Treemacs
(define-key evil-normal-state-map (kbd "C-\\") '+treemacs/toggle)
(setq treemacs-default-visit-action 'treemacs-visit-node-close-treemacs)
(with-eval-after-load 'treemacs
  (define-key treemacs-mode-map (kbd "ESC") 'treemacs-quit)
)


;;;; -- Terminal specific settings -- ;;;;

(unless window-system
  (global-set-key (kbd "<mouse-4>") 'scroll-down-line) ;; \_ Mouse
  (global-set-key (kbd "<mouse-5>") 'scroll-up-line)   ;; /  scroll
  (define-key evil-normal-state-map (kbd "C-_") 'evilnc-comment-or-uncomment-lines)
  (map! :v "C-y" #'clipetty-kill-ring-save)
  )


;;;; -- Spell Checking -- ;;;;

(global-set-key (kbd "<f6>") 'flyspell-mode)


;;;; -- Language-specific -- ;;;;

;; Markdown
(custom-set-variables
  '(markdown-toc-header-toc-start "<!-- TOC -->")
  '(markdown-toc-header-toc-end "<!-- /TOC -->")
  '(markdown-toc-indentation-space 4)
)


;;;; -- Hooks -- ;;;;

(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (add-hook 'before-save-hook #'+format/buffer)
            ))

(add-hook 'markdown-mode-hook
          (lambda ()
            (add-hook 'before-save-hook #'markdown-toc-refresh-toc)
            ))

(add-hook 'python-mode-hook
          (lambda ()
            (add-hook 'before-save-hook #'+python/optimize-imports)
            ))

(add-hook 'ruby-mode-hook #'format-all-mode)
(setq-hook! 'ruby-mode-hook +format-with 'rubocop)

(add-hook 'terraform-mode-hook #'terraform-format-on-save-mode)

(remove-hook 'tty-setup-hook 'doom-init-clipboard-in-tty-emacs-h)


;;;; -- Other -- ;;;;

(setq confirm-kill-emacs nil)
(setq projectile-enable-caching nil)
(global-auto-revert-mode t)
